import os
import jinja2
import webapp2
from google.appengine.ext import blobstore
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext.webapp import blobstore_handlers

class Document(ndb.Model):
    file_name = ndb.StringProperty()
    is_folder = ndb.BooleanProperty()
    blobkey = ndb.BlobKeyProperty()
	
class Folder(ndb.Model):
    parent_folder = ndb.StringProperty()
    documents = ndb.StructuredProperty(Document, repeated = True)

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True
)

class BasePage(object):
	def assert_fetch(self,name,key):		
		folder = ndb.Key(name,key).get()
		return folder

class MainPage(webapp2.RequestHandler, BasePage):
	def get(self):
		self.response.headers['content-type'] = 'text/html'
		url = ''
		url_string = ''
		folder = None
		user = users.get_current_user()

		if user:
			url = users.create_logout_url(self.request.uri)
			url_string = 'logout'
			folder = self.assert_fetch('Folder',user.user_id())
			if folder == None:
				folder = Folder(id = user.user_id())
				folder.parent_folder = user.user_id()
				folder.put()
			elif self.request.get('cwd'):
				folder_name = self.request.get('cwd')
				folder = self.assert_fetch('Folder',folder_name)
			if folder == None:
				folder = self.assert_fetch('Folder',user.user_id())

		else:
			url = users.create_login_url(self.request.uri)
			url_string = 'login'

		template_values = {
            'url' : url,
            'url_string' : url_string,
            'user': user,
            'folder': folder,
            'query_string': self.request.query_string,
            'upload_url': blobstore.create_upload_url('/upload'),
        }

		template = JINJA_ENVIRONMENT.get_template('main.html')
		self.response.write(template.render(template_values))

	def post(self):
		def del_folder(folder):
			for document in folder.documents:
				if document.is_folder:
					temp = self.assert_fetch('Folder',folder.key.id() + '/' + document.file_name)
					del_folder(temp)
				else:
					blobstore.delete(document.blobkey)
			folder.key.delete()
		
		if self.request.get('add_folder'):
			folder_name = self.request.get('file_name').strip()
			parent_folder = self.request.get('parent_folder')
			folder = self.assert_fetch('Folder', parent_folder + '/' + folder_name)

			if not folder and folder_name:
				folder = Folder(id = parent_folder + '/' + folder_name)
				folder.parent_folder = parent_folder
				folder.put()	
				folder = self.assert_fetch('Folder',parent_folder)

				for document in folder.documents:
					if document.file_name == folder_name:
						return self.redirect("/")

				folder.documents.append(Document(file_name=folder_name,is_folder=True))
				folder.put()
				
		elif self.request.get('del_folder'):
			folder_name = self.request.get('file_name')
			parent_folder = self.request.get('parent_folder')
			folder = self.assert_fetch('Folder', parent_folder + '/' + folder_name)

			if folder:
				del_folder(folder)				
				folder = self.assert_fetch('Folder', parent_folder)
				my_documents = []
				for document in folder.documents:
					if document.file_name != folder_name:
						my_documents.append(document)
				folder.documents = my_documents
				folder.put()

		elif self.request.get('del_document'):
			file_name = self.request.get('file_name')
			parent_folder = self.request.get('parent_folder')
			folder = self.assert_fetch('Folder', parent_folder)

			my_documents = []
			for document in folder.documents:
				if document.file_name == file_name:
					blobstore.delete(document.blobkey)
				else:
					my_documents.append(document)

			folder.documents = my_documents
			folder.put()

		self.redirect('/?cwd=' + self.request.get('parent_folder'))
		
class UploadHandler(blobstore_handlers.BlobstoreUploadHandler, BasePage):
	def post(self):
		user = users.get_current_user()
		if not user:
			return self.redirect("/")
			
		upload   = self.get_uploads()[0]
		blobinfo = blobstore.BlobInfo(upload.key())
		file_name = blobinfo.filename
		parent_folder = self.request.get("parent_folder")
		
		folder = self.assert_fetch('Folder',parent_folder)
		check = True
		for document in folder.documents:
			if document.file_name == file_name:
				check = False
		if check == True:
			document = Document(file_name=file_name, is_folder=False, blobkey=upload.key())
			folder.documents.append(document)
		else:
			return self.redirect("/?cwd=" + self.request.get("parent_folder"))
		folder.put()
		self.redirect("/?cwd=" + self.request.get("parent_folder"))

class DownloadHandler(blobstore_handlers.BlobstoreDownloadHandler, BasePage):
	def get(self):
		file_name = self.request.get('file_name')
		parent_folder = self.request.get('parent_folder')
		folder = self.assert_fetch('Folder',parent_folder)
		for document in folder.documents:
			if document.file_name == file_name:
				break;
		return self.send_blob(document.blobkey)		

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/upload', UploadHandler),
	('/download', DownloadHandler),
])
